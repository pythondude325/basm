; Comments are written with a preceding semicolon

; Constants can be defined and used in expressions
const a = 5

; Binary, Octal, and Hexadecimal constants can be used aswell
const my_binary_number = 0b110010
const my_octal_number = 0o0712
const my_hexadecimal_number = 0xA32

; Labels are used to mark locations in the binary
myLabel:

; db directives are used to embed data in the resulting binary.
; They can use strings, integers, or constants
db "Hello World", 0, my_binary_number

; Finally, instructions can be used like so:
mov ac, 5
; all language keywords are case insensitive, so the mnemonics may be in all
; caps if that is your preference
ADD 2
